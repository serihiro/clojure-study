(ns fizzbuzz.core)

(defn fizzbuzz* [x]
  (if (zero? (rem x 15))
    "FizzBuzz"
    (if (zero? (rem x 3))
      "Fizz"
      (if (zero? (rem x 5))
        "Buzz"
        x))))

(defn fizzbuzz [x]
  ;ワシが書いたやつ
  ;(for [n (range 1 (+ x 1))] (fizzbuzz* n))
  ;あやぴー先生
  (map fizzbuzz* (range 1 (+ x 1)))
  )


;第二引数以降のリスト（のようなもの）から同じインデックスのやつを取り出して足し込むぞ
(map +
     (range 10)
     (range 10 20))

;例えばこれは引数を２つ渡すのでエラーになるぞ
;(map clojure.string/upper-case '("a", "b", "c") '("d", "e", "f"))

(let [[a & more :as org] (range 10)]                        ;; a が先頭だけを取り、 & を挟むことで残りを表現出来ます。 :as org は元のリストを org として参照することが出来ます。
  (println org)
  (println a)
  (println more))
