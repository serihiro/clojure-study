(ns fizzbuzz.core-test
  (:require [clojure.test :refer :all]
            [fizzbuzz.core :refer :all]))

(deftest fizzbuzz-test
  (testing "FizzBuzz test"
    (are [arg a] (= (fizzbuzz* arg) a)
                 1 1
                 3 "Fizz"
                 5 "Buzz"
                 15 "FizzBuzz"
                 ))
  )
