(ns todo-clj.core
  (:require [compojure.core :refer [routes]]
            [ring.adapter.jetty :as server]
            [todo-clj.handler.main :refer [main-routes]]
            [todo-clj.handler.todo :refer [todo-routes]]
            [todo-clj.middleware :refer [wrap-dev]]
            ))

;;Routing
(def app
  (routes
    todo-routes
    main-routes
    wrap-dev))

;;Server control
(defonce server (atom nil))

(defn start-server []
  (when-not @server
    (reset! server (server/run-jetty #'app {:port 3000 :join? false}))))

(defn stop-server []
  (when @server
    (.stop @server)
    (reset! server nil)))

(defn restart-server []
  (when @server
    (stop-server)
    (start-server)))

;;Application Logic

;;Routing
;(def routes
;  {"/"     home
;   "/todo" todo-index})
;
;(defn match-route [uri]
;  (get routes uri))
;
;(defn handler [req]
;  (let [uri (:uri req)
;        maybe-fn (match-route uri)]
;    (if maybe-fn
;      (maybe-fn req)
;      (not-found))))
